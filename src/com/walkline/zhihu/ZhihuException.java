package com.walkline.zhihu;

public class ZhihuException extends Exception
{
	public ZhihuException() {super();}

	public ZhihuException(String message) {super(message);}
}