package com.walkline.zhihu.dao;

import org.json.me.JSONObject;

import com.walkline.app.ZhihuAppConfig;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.ThemeStory;

public class ZhihuThemeStory extends ZhihuObject implements ThemeStory
{
	private String _title = "";
	private String _url = "";
	private int _id = 0;
	private int _type = 0;
	private String _share_url = "";

	public ZhihuThemeStory(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject sectionStory = jsonObject;
		if (sectionStory != null)
		{
			_type = sectionStory.optInt("type");
			_id = sectionStory.optInt("id");
			_share_url = sectionStory.optString("share_url");
			_title = sectionStory.optString("title");

			if (_id != 0) {_url = ZhihuAppConfig.queryStoryDetailsRequestURL + _id;}
		}
	}

	public String getTitle() {return _title;}

	public String getUrl() {return _url;}

	public int getId() {return _id;}

	public int getType() {return _type;}

	public String getShareUrl() {return _share_url;}
}