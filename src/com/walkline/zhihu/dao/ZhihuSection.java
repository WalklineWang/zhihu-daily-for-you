package com.walkline.zhihu.dao;

import org.json.me.JSONObject;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.Section;

public class ZhihuSection extends ZhihuObject implements Section
{
	private String _name = "";
	private String _description = "";
	private String _thumbnail = "";
	private int _id = 0;

	public ZhihuSection(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject section = jsonObject;
		if (section != null)
		{
			_id = section.optInt("id");
			_name = section.optString("name");
			_description = section.optString("description");
			_thumbnail = section.optString("thumbnail");
		}
	}

	public int getId() {return _id;}

	public String getName() {return _name;}

	public String getDescription() {return _description;}

	public String getThumbnail() {return _thumbnail;}
}