package com.walkline.zhihu.dao;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.SectionStory;

public class ZhihuSectionStory extends ZhihuObject implements SectionStory
{
	private String _title = "";
	private String _url = "";
	private String _image = "";
	private int _id = 0;
	private String _display_date = "";
	private String _date = "";

	public ZhihuSectionStory(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject sectionStory = jsonObject;
		if (sectionStory != null)
		{
			_display_date = sectionStory.optString("display_date");
			_title = sectionStory.optString("title");
			_url = sectionStory.optString("url");
			_date = sectionStory.optString("date");
			_id = sectionStory.optInt("id");

			JSONArray images = sectionStory.optJSONArray("images");
			if (images != null)
			{
				try {_image = (String) images.get(0);} catch (JSONException e) {}
			}
		}
	}

	public String getTitle() {return _title;}

	public String getUrl() {return _url;}

	public String getImage() {return _image;}

	public int getId() {return _id;}

	public String getDisplayDate() {return _display_date;}

	public String getDate() {return _date;}
}