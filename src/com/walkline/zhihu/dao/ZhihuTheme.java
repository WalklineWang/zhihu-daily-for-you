package com.walkline.zhihu.dao;

import org.json.me.JSONObject;

import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.Theme;

public class ZhihuTheme extends ZhihuObject implements Theme
{
	private String _name = "";
	private String _description = "";
	private String _image = "";
	private int _id = 0;
	private int _color = 0;

	public ZhihuTheme(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject theme = jsonObject;
		if (theme != null)
		{
			_id = theme.optInt("id");
			_name = theme.optString("name");
			_description = theme.optString("description");
			_color = theme.optInt("color");
			_image = theme.optString("image");
		}
	}

	public int getId() {return _id;}

	public String getName() {return _name;}

	public String getDescription() {return _description;}

	public String getImage() {return _image;}

	public int getColor() {return _color;}
}