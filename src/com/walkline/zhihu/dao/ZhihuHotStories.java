package com.walkline.zhihu.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.HotStories;
import com.walkline.zhihu.inf.HotStory;

public class ZhihuHotStories extends ZhihuObject implements HotStories
{
	private Vector _hot_stories = new Vector();

	public ZhihuHotStories(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject hotStories = jsonObject;
		if (hotStories != null)
		{
			JSONArray storiesArray = hotStories.optJSONArray("recent");
			if (storiesArray != null)
			{
				for (int i=0; i<storiesArray.length(); i++)
				{
					try {
						JSONObject storyObject = (JSONObject) storiesArray.get(i);

						HotStory story = new ZhihuHotStory(zhihu, storyObject);
						if (story != null) {_hot_stories.addElement(story);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public Vector getHotStories() {return _hot_stories;}
}