package com.walkline.zhihu.dao;

import org.json.me.JSONObject;

import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;

public class ZhihuObject implements com.walkline.zhihu.inf.Object
{
	protected ZhihuSDK zhihu;
	protected JSONObject jsonObject;

	public ZhihuObject(ZhihuSDK pZhihu, JSONObject pJsonObject) throws ZhihuException
	{
		if ((pZhihu == null) || (pJsonObject == null)) {
			throw new ZhihuException("Unable to create ZhihuSDK ZhihuObject.");
		}

		zhihu = pZhihu;
		jsonObject = pJsonObject;
	}
}