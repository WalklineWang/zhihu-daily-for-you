package com.walkline.zhihu.dao;

import java.util.Vector;
import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.ThemeStories;
import com.walkline.zhihu.inf.ThemeStory;

public class ZhihuThemeStories extends ZhihuObject implements ThemeStories
{
	private Vector _stories = new Vector();
	private int _color = 0;
	private String _name = "";
	private String _description = "";
	private String _background = "";
	private String _image = "";
	//private Vector _editors = new Vector();
	private String _image_source = "";

	public ZhihuThemeStories(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject themeStories = jsonObject;
		if (themeStories != null)
		{
			_description = themeStories.optString("description");
			_background = themeStories.optString("background");
			_color = themeStories.optInt("color");
			_name = themeStories.optString("name");
			_image = themeStories.optString("image");
			_image_source = themeStories.optString("image_source");

			JSONArray storiesArray = themeStories.optJSONArray("stories");
			if (storiesArray != null)
			{
				for (int i=0; i<storiesArray.length(); i++)
				{
					try {
						JSONObject newsObject = (JSONObject) storiesArray.get(i);

						ThemeStory themeStory = new ZhihuThemeStory(zhihu, newsObject);
						if (themeStory != null) {_stories.addElement(themeStory);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public Vector getStories() {return _stories;}

	public String getName() {return _name;}

	public String getDescription() {return _description;}

	public String getBackground() {return _background;}

	public int getColor() {return _color;}

	public String getImage() {return _image;}

	public String getImageSource() {return _image_source;}
}