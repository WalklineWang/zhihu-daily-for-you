package com.walkline.zhihu.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.Theme;
import com.walkline.zhihu.inf.Themes;

public class ZhihuThemes extends ZhihuObject implements Themes
{
	private int _limit = 0;
	private String _subscribed = "";
	private Vector _others = new Vector();

	public ZhihuThemes(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject themes = jsonObject;
		if (themes != null)
		{
			_limit = themes.optInt("limit");
			_subscribed = themes.optJSONArray("subscribed").toString();

			JSONArray themesArray = themes.optJSONArray("others");
			if (themesArray != null)
			{
				for (int i=0; i<themesArray.length(); i++)
				{
					try {
						JSONObject themeObject = (JSONObject) themesArray.get(i);

						Theme theme = new ZhihuTheme(zhihu, themeObject);
						if (theme != null) {_others.addElement(theme);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public Vector getSections() {return _others;}

	public int getLimit() {return _limit;}

	public String getSubscribed() {return _subscribed;}

	public Vector getOthers() {return _others;}
}