package com.walkline.zhihu.dao;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.util.Function;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.SectionStories;
import com.walkline.zhihu.inf.SectionStory;

public class ZhihuSectionStories extends ZhihuObject implements SectionStories
{
	private int _timestamp = 0;
	private Vector _stories = new Vector();
	private String _name = "";

	public ZhihuSectionStories(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject sectionStories = jsonObject;
		if (sectionStories != null)
		{
			_timestamp = sectionStories.optInt("timestamp");

			JSONArray storiesArray = sectionStories.optJSONArray("stories");
			if (storiesArray != null)
			{
				for (int i=0; i<storiesArray.length(); i++)
				{
					try {
						JSONObject newsObject = (JSONObject) storiesArray.get(i);

						SectionStory sectionStory = new ZhihuSectionStory(zhihu, newsObject);
						if (sectionStory != null) {_stories.addElement(sectionStory);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public Vector getStories() {return _stories;}

	public int getTimeStamp() {return _timestamp;}

	public String getName() {return _name;}
}