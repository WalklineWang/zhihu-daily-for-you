package com.walkline.zhihu.dao;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.walkline.app.ZhihuAppConfig;
import com.walkline.zhihu.ZhihuException;
import com.walkline.zhihu.ZhihuSDK;
import com.walkline.zhihu.inf.Story;

public class ZhihuStory extends ZhihuObject implements Story
{
	private String _title = "";
	private String _url = "";
	private String _image = "";
	private String _share_url = "";
	private String _thumbnail = "";
	private String _ga_prefix = "";
	private int _type = 0;
	private int _id = 0;
	//private boolean _is_top_topic = false;

	public ZhihuStory(ZhihuSDK zhihu, JSONObject jsonObject) throws ZhihuException
	{
		super(zhihu, jsonObject);

		JSONObject story = jsonObject;
		if (story != null)
		{
			_title = story.optString("title");
			_share_url = story.optString("share_url");
			_ga_prefix = story.optString("ga_prefix");
			_type = story.optInt("type");
			_id = story.optInt("id");
			_image = story.optString("image");

			JSONArray images = story.optJSONArray("images");
			if (images != null)
			{
				try {_thumbnail = (String) images.get(0);} catch (JSONException e) {}
			}

			if (_id != 0) {_url = ZhihuAppConfig.queryStoryDetailsRequestURL + _id;}
		}
	}

	public String getTitle() {return _title;}

	public String getUrl() {return _url;}

	public String getImage() {return _image;}

	public String getShareUrl() {return _share_url;}

	public String getThumbnail() {return _thumbnail;}

	public String getGaPrefix() {return _ga_prefix;}

	public int getType() {return _type;}

	public int getId() {return _id;}

	//public boolean isTopStory1111() {return _is_top_topic;}

	//public void setTopStory(boolean value) {_is_top_topic = value;}
}