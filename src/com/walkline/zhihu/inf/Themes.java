package com.walkline.zhihu.inf;

import java.util.Vector;

/**
 * 知乎主题分类集合
 * @author Walkline
 *
 */
public interface Themes extends com.walkline.zhihu.inf.Object
{
	public int getLimit();

	public String getSubscribed();

	public Vector getOthers();
}