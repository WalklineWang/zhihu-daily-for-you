package com.walkline.zhihu.inf;


public interface Story
{
	public String getTitle();		//新闻标题

	public String getShareUrl();	//原文地址

	public String getGaPrefix();	//Unknown

	//public Vector getImages();		//封面缩略图集合

	public String getThumbnail();	//封面预览图

	public int getType();			//Unknown

	public int getId();				//新闻ID

	public String getUrl();			//API访问地址

	//public String getImageSource();	//图片来源

	public String getImage();		//热门新闻大图

	//public boolean isTopStory();

	//public void setTopStory(boolean value);
}