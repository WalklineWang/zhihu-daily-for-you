package com.walkline.zhihu.inf;


public interface HotStory
{
	public String getTitle();		//新闻标题

	public String getThumbnail();	//封面预览图

	public int getNewsId();				//新闻ID

	public String getUrl();			//API访问地址
}