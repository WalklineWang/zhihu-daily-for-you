package com.walkline.zhihu.inf;

/**
 * 知乎专题详情
 * @author Walkline
 */
public interface Section
{
	public String getName();

	public String getDescription();

	public int getId();

	public String getThumbnail();
}