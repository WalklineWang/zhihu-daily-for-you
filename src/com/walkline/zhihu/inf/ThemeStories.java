package com.walkline.zhihu.inf;

import java.util.Vector;

public interface ThemeStories extends com.walkline.zhihu.inf.Object
{
	public Vector getStories();

	public String getDescription();

	public String getBackground();

	public int getColor();

	public String getName();

	public String getImage();

	//public Vector getEditors();

	public String getImageSource();
}