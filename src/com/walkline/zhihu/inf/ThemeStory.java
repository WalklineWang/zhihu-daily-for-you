package com.walkline.zhihu.inf;


public interface ThemeStory
{
	public String getTitle();

	public int getId();

	public String getUrl();

	public int getType();

	public String getShareUrl();
}