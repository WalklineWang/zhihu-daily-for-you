package com.walkline.zhihu.inf;


public interface SectionStory
{
	public String getTitle();		//新闻标题

	public int getId();				//新闻ID

	public String getUrl();			//API访问地址

	public String getImage();		//热门新闻大图

	public String getDisplayDate();

	public String getDate();

	//public boolean isTopStory();

	//public void setTopStory(boolean value);
}