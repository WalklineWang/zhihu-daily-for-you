package com.walkline.zhihu.inf;

import java.util.Vector;

public interface SectionStories extends com.walkline.zhihu.inf.Object
{
	public int getTimeStamp();

	public Vector getStories();

	public String getName();
}