package com.walkline.zhihu;

import com.walkline.zhihu.inf.DailyStories;
import com.walkline.zhihu.inf.HotStories;
import com.walkline.zhihu.inf.Section;
import com.walkline.zhihu.inf.SectionStories;
import com.walkline.zhihu.inf.Sections;
import com.walkline.zhihu.inf.StoryDetails;
import com.walkline.zhihu.inf.Theme;
import com.walkline.zhihu.inf.ThemeStories;
import com.walkline.zhihu.inf.Themes;

public interface AsyncCallback
{
	public void onException(Exception e, Object state);
	public void onComplete(DailyStories value, Object state);
	public void onComplete(HotStories value, Object state);
	public void onComplete(StoryDetails value, Object state);
	public void onComplete(Sections value, Object state);
	public void onComplete(Section value, Object state);
	public void onComplete(SectionStories value, Object state);
	public void onComplete(Themes value, Object state);
	public void onComplete(Theme value, Object state);
	public void onComplete(ThemeStories value, Object state);
}