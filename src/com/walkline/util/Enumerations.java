package com.walkline.util;

public class Enumerations
{
	public static class RefreshActions
	{
		public static final int DAILYSTORIES = 0;
		public static final int HOTSTORIES = 1;
		public static final int STORYDETAILS = 2;
		public static final int SECTIONS = 3;
		public static final int SECTIONSTORIES = 4;
		public static final int THEMES = 5;
		public static final int THEMESTORIES = 6;
		public static final int SECTIONSTORYDETAILS = 7;
	}

	public static class UploadMethod
	{
		public static final String[] choicesUploadMethod = {"PUT", "POST"};

		public static final int PUT = 0;
		public static final int POST = 1;

		public static final int DEFAULT_METHOD = PUT;
	}

	public static class ShortcutsKey
	{
		public static final String[] choicesShortcutKeys = {"", "E", "F", "G", "I", "J", "K", "Q", "R", "W", "X", "Y", "Z"};
		
		public static final int DEFAULT_KEY = 12;
	}
}