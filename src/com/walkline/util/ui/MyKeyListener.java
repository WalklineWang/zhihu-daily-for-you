package com.walkline.util.ui;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.KeyListener;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.component.Dialog;

public class MyKeyListener implements KeyListener
{
	private Manager _manager;

	public MyKeyListener(Manager browserFieldManager)
	{
		try {_manager = browserFieldManager;} catch (Exception e) {}
	}

    private void showExitDialog()
    {
		String[] yesno = {"是 (Y\u0332)", "否 (N\u0332)"};
		Dialog showDialog = new Dialog("确认退出知乎日报？", yesno, null, 0, Bitmap.getPredefinedBitmap(Bitmap.QUESTION), 0);

		showDialog.doModal();
		if (showDialog.getSelectedValue() == 0) {System.exit(0);}
    }

	public boolean keyChar(char character, int status, int time)
	{
		int currentScrollPosition = 0;
		int pageTopPosition = 0;
		int pageBottomPosition  = 0;
		int scrollAmount = 0; 
		int targetScrollPosition = 0;

		try
		{
			if (_manager == null) {return false;}

			currentScrollPosition = _manager.getManager().getVerticalScroll();
			pageTopPosition =  _manager.getTop();
			pageBottomPosition = _manager.getHeight();		
			scrollAmount = _manager.getVisibleHeight();

			switch(character)
			{
				case Characters.SPACE:
					//SPACE = down; SHIFT + SPACE = up 
		   			if ((status & KeypadListener.STATUS_SHIFT) == 0)
		   			{
		   				_manager.getScreen().scroll(Manager.DOWNWARD);
		   				targetScrollPosition = ((currentScrollPosition + scrollAmount) > pageBottomPosition) ? pageBottomPosition : (currentScrollPosition + scrollAmount);
		   				_manager.getManager().setVerticalScroll(targetScrollPosition, true);
		   			} else {
		   				_manager.getScreen().scroll(Manager.UPWARD);
		   				targetScrollPosition = ((currentScrollPosition - scrollAmount) < pageTopPosition) ? pageTopPosition : (currentScrollPosition - scrollAmount);
		   				_manager.getManager().setVerticalScroll(targetScrollPosition, true);
		   			}
		   			return true;
		   		case Characters.LATIN_CAPITAL_LETTER_T:
		   		case Characters.LATIN_SMALL_LETTER_T:
		   			_manager.getScreen().scroll(Manager.TOPMOST);

		   			targetScrollPosition = pageTopPosition;
		   			_manager.getManager().setVerticalScroll(targetScrollPosition, true);
		   			return true;
		   		case Characters.LATIN_CAPITAL_LETTER_B:
		   		case Characters.LATIN_SMALL_LETTER_B:
		   			_manager.getScreen().scroll(Manager.BOTTOMMOST);
		   			targetScrollPosition = (pageBottomPosition - scrollAmount);
		   			_manager.getManager().setVerticalScroll(targetScrollPosition, true);
		   			return true;
				case Characters.LATIN_CAPITAL_LETTER_Q:
				case Characters.LATIN_SMALL_LETTER_Q:
					showExitDialog();
					return true;
			}
		} catch (Exception e) {}

		return false;
	}

	public boolean keyDown(int keycode, int time) {return false;}
	public boolean keyRepeat(int keycode, int time) {return false;}
	public boolean keyStatus(int keycode, int time) {return false;}
	public boolean keyUp(int keycode, int time) {return false;}
}