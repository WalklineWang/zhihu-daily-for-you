package com.walkline.util.ui;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;

import com.walkline.app.ZhihuAppConfig;
import com.walkline.util.GPATools;

public class MyTitleBitmapField extends BitmapField
{
	private Bitmap _bitmap;
	private Bitmap _newBitmap;
	private String _title;
	private String _image_source;

	private MyTitleLabelField _labelTitle;
	private MyTitleLabelField _labelImageSource;

	private int _bitmapPadding = 0;
	private int _targetWidth = 0;
	private int _targetHeight = 360;

	private String deviceName = DeviceInfo.getDeviceName();
	private boolean isTouchDevice = (deviceName.equalsIgnoreCase("9800") || deviceName.equalsIgnoreCase("9810") || deviceName.equalsIgnoreCase("9500") || deviceName.equalsIgnoreCase("9520") || deviceName.equalsIgnoreCase("9850") || deviceName.equalsIgnoreCase("9550")) ? true : false;

	private Bitmap _maskBitmap;

	public MyTitleBitmapField() {super();}

	public void setTitle(String title)
	{
		_title = title;
		_labelTitle = new MyTitleLabelField(_title);
		_labelTitle.setFont(ZhihuAppConfig.FONT_STORY_DETAIL_BITMAP_TITLE);
		_labelTitle.layout(getWidth() - 20 - 20, getHeight());
	}

	public void setImageSource(String source)
	{
		_image_source = source;
		_labelImageSource = new MyTitleLabelField("图片：" + _image_source);
		_labelImageSource.setFont(ZhihuAppConfig.FONT_STORY_DETAIL_IMAGE_SOURCE);
		_labelImageSource.layout(getWidth(), getHeight());
	}

	public void setBitmap(Bitmap bitmap)
	{
		_bitmap = bitmap;

		super.setBitmap(bitmap);
	}

	protected void layout(int width, int height)
	{
		if (isTouchDevice)
		{
			_bitmapPadding = Display.getHeight() > 360 ? 80 : 120;
		} else {
			_bitmapPadding = Display.getHeight() < 480 ? 120 : 160;
		}

		_targetWidth = Display.getWidth();
		_newBitmap = GPATools.ResizeTransparentBitmap(_bitmap, _targetWidth, _targetWidth, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_STRETCH);

        if (_newBitmap != null) {_targetHeight = _newBitmap.getHeight() - _bitmapPadding * 2; getMaskBitmap(_newBitmap);}

        //if (_labelTitle != null) {_labelTitle.layout(width - 20 - 20, height);}

        //if (_labelImageSource != null) {_labelImageSource.layout(width, height);}

        setExtent(_targetWidth, _targetHeight);
	}

	protected void paint(Graphics g)
	{
       	if (_newBitmap != null)
       	{
       		g.drawBitmap(0, -_bitmapPadding, _newBitmap.getWidth(), _newBitmap.getHeight() - _bitmapPadding, _newBitmap, 0, 0);
       		g.drawBitmap(0, 0, _newBitmap.getWidth(), _newBitmap.getHeight(), _maskBitmap, 0, 0);
       	}

    	if (_labelTitle != null)
    	{
            try
            {
            	g.setFont(ZhihuAppConfig.FONT_STORY_DETAIL_BITMAP_TITLE);
            	g.setColor(Color.WHITE);
            	g.pushRegion(20, _targetHeight - _labelTitle.getHeight() - (_labelImageSource != null ? _labelImageSource.getHeight() : 0) - 10, _labelTitle.getWidth(), _labelTitle.getHeight(), 0, 0);
            	_labelTitle.paint(g);
            } finally {g.popContext();}
    	}

    	if (_labelImageSource != null)
    	{
            try
            {
            	g.setFont(ZhihuAppConfig.FONT_STORY_DETAIL_IMAGE_SOURCE);
            	g.setColor(0xa1a0a1);
            	g.pushRegion(getWidth() - _labelImageSource.getWidth() - 20, _targetHeight - _labelImageSource.getHeight() - 10, _labelImageSource.getWidth(), _labelImageSource.getHeight(), 0, 0);
            	_labelImageSource.paint(g);
            } finally {g.popContext();}
    	}
	}

    //protected void paintBackground(Graphics g) {super.paintBackground(g);}

	private void getMaskBitmap(Bitmap source)
	{
   		int width = source.getWidth();
   		int height = source.getHeight() - _bitmapPadding;

   		_maskBitmap = new Bitmap(width, height);
   		int data[]=new int[width * height];

   		_maskBitmap.setARGB(data, 0, width, 0, 0, width, height);
   		Graphics graphics = Graphics.create(_maskBitmap);
   		graphics.setColor(Color.BLACK);
   		graphics.setGlobalAlpha(70);
   		graphics.fillRect(0, 0, width, height);
	}

	class MyTitleLabelField extends LabelField
	{
	    public MyTitleLabelField(String text) {super(text, 0);}

		public void layout(int width, int height) {super.layout(width, height);}   

		public void paint(Graphics g) {super.paint(g);}
	}
}